library(tidyverse)
insurance <- read.csv("insurance.csv")
colnames(insurance)
mean = mean(insurance$age)
mean
stdD <- sd(insurance$age)
stdD
mean = mean(insurance$charges)
mean
stdD <- sd(insurance$charges)
stdD
range(insurance$charges)
range(insurance$bmi)
table(insurance$sex)
prop.table(table(insurance$sex))
table(insurance$children)
prop.table(table(insurance$children))
table(insurance$smoker)
prop.table(table(insurance$smoker))
table(insurance$region)
prop.table(table(insurance$region))
cor(insurance$bmi,insurance$charges, method = c("kendall"))
cor.test(insurance$bmi,insurance$charges, method="kendall")

